#!/bin/bash

# enable additional repositories for things like ubuntu-restricted-extras
cp files/sources.list /etc/apt/sources.list
apt-get update

#apt-get install -y cowsay

function mandosay() {
	#cowsay -f ./mando.cow $1
	printf "\n$1\n"
}

# setting to accept eula in ubuntu-restricted-extras
echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | debconf-set-selections

yes | apt-get install ubuntu-restricted-extras vlc


# create user
clear
mandosay "Enter your new password"
adduser --gecos "" mando

clear
mandosay "This is the way."